#Task HRtec

## PDF with information about task
- [document PDF](https://bitbucket.org/adam-benchmark/feed_rss_to_csv/downloads/Rekrutacja-PHP.pdf)

##Run command line

### Run via Make Utils
This is macro for Linux in `Makefile` in root catalog
```
make run-simple

make run-extended

make unit-tests

```
###Run via docker
```
docker-compose run --rm --no-deps php-cli php src/console.php csv:simple https://blog.nationalgeographic.org/rss csv/simple_export.csv

docker-compose run --rm --no-deps php-cli php src/console.php csv:expanded https://blog.nationalgeographic.org/rss csv/extended_export.csv
```
###Command for run on system with PHP and Server

```
php console.php csv:simple https://blog.nationalgeographic.org/rss csv/simple_export.csv
```
##Run docker
```
docker-compose up -d
```
##Deep into container
```
docker exec -it hrtec_docker_php-cli_1 bash
```
# Docker Environment / PHP 7.4 console / composer / phpunit 
https://github.com/dnegometyanov/docker-php74-console-composer-phpunit
Blank docker project for console php 7.4 projects with composer and phpunit.

## Prerequisites

Install Docker and optionally Make utility.

Commands from Makefile could be executed manually in case Make utility is not installed.

## Build container and install composer dependencies

    make build

## Build container and install composer dependencies

If dist files are not copied to actual destination, then
    
    make copy-dist-configs
        
## Run application

### Runs container and executes console application | simply varinat.

    make run-simply

### Runs container and executes console application | simply extended.

    make run-extended

## Run unit tests

Runs container and executes unit tests.

    make unit-tests
