<?php

namespace AdamKsiazekRekrutacjaHRtecTest\Services\SiteData;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Services\SiteData\SiteDataService;
use AdamKsiazekRekrutacjaHRtec\Services\Tasks\ReadFeed;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use SimpleXMLElement;

class SiteDataServiceTest extends TestCase
{
    public function testReadFeedConstruct(): void
    {
        $siteDataService = new SiteDataService();

        $this->assertInstanceOf(SiteDataService::class, $siteDataService);
    }

    /**
     * @return \string[][]
     */
    public function providerSimpleXmlElementByContent()
    {
        return [
          ['title', '<item><title>title</title></item>']
        ];
    }

    /**
     * @dataProvider providerSimpleXmlElementByContent
     */
    public function testSimpleXmlElementByContent($expected, $input)
    {
        $object = new SiteDataService();
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('simpleXmlElementByContent');
        $method->setAccessible( true );
        $testResult = $method->invokeArgs( $object, array($input) );
        $this->assertInstanceOf(\SimpleXMLElement::class, $testResult);
        $this->assertEquals( $expected, $testResult->title );
    }

    public function providerExtract()
    {
        $content = '<site><channel><item><title>title</title><description>desc</description></item></channel></site>';
        $simpleXMLElement = new SimpleXMLElement($content);
        return [
            ['title', $simpleXMLElement]
        ];
    }

    /**
     * @dataProvider providerExtract
     */
    public function testExtract($expected, $input)
    {
        $object = new SiteDataService();
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('extract');
        $method->setAccessible(true);
        $testResult = $method->invokeArgs($object, array($input));
        $this->assertInstanceOf(\ArrayObject::class, $testResult);
        $this->assertEquals( $expected, $testResult[0]->title );
    }

    public function testInit()
    {
        $argvMock = [
            "console.php",
            "csv:extended",
            "",
            "simple_export.csv"
        ];
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argvMock);
        $expectedObject = new \ArrayObject();
        $mock = $this->getMockBuilder(SiteDataService::class)
            ->setMethods(['init'])
            ->getMock();

        $mock->expects($this->once())
            ->method('init');

        $mock->init($inputArgumentsDTO);
    }

    public function providerInitSiteUrlEmpty()
    {
        $argvMock = [
            "console.php",
            "csv:extended",
            "",
            "simple_export.csv"
        ];
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argvMock);

        return [
          ['', $inputArgumentsDTO]
        ];

    }

    /**
     * @dataProvider providerInitSiteUrlEmpty
     */
    public function testInitSiteUrlEmpty($expect, $input)
    {
        $object = new SiteDataService();
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('init');
        $testResult = $method->invokeArgs( $object, array($input) );
        $this->assertInstanceOf(\ArrayObject::class, $testResult);

    }
}
