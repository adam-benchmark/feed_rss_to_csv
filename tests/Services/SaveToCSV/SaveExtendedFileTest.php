<?php

namespace AdamKsiazekRekrutacjaHRtecTest\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\FeedElementDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveExtendedFile;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveSimpleFile;
use PHPUnit\Framework\TestCase;

class SaveExtendedFileTest extends TestCase
{
    /**
     * @return array[]
     */
    public function providerSaveExtended(): array
    {
        $argvMockExtended = [
            "console.php",
            "csv:extended",
            "https://blog.nationalgeographic.org/rss",
            "csv/extended_export.csv"
        ];
        return [
            ['SaveExtendedFile | done', $argvMockExtended]
        ];
    }

    /**
     * @dataProvider providerSaveExtended
     */
    public function testSaveExtended($expect, $input): void
    {
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($input);

        $feedElementDTO = new FeedElementDTO();
        $feedElementDTO->title = 'jakis title';
        $feedElementDTO->description = 'jakis description';

        $feedElements = new \ArrayObject([$feedElementDTO]);

        $saveSimpleFile = new SaveExtendedFile();
        $result = $saveSimpleFile->save($inputArgumentsDTO, $feedElements);
        $this->assertEquals($expect, $result);
    }

    public function providerSaveExtendedEmptyFeedElements()
    {
        $argvMockExtended = [
            "console.php",
            "csv:extended",
            "https://blog.nationalgeographic.org/rss",
            "csv/extended_export.csv"
        ];
        $feedElements = new \ArrayObject([]);
        return [
            ['Error | feed is empty', [$argvMockExtended, $feedElements]]
        ];
    }

    /**
     * @dataProvider providerSaveExtendedEmptyFeedElements
     */
    public function testSaveSimpleEmptyFeedElements($expect, $input)
    {
        $saveExtendedFile = new SaveExtendedFile();
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($input[0]);
        $result = $saveExtendedFile->save($inputArgumentsDTO, $input[1]);
        $this->assertEquals($expect, $result);
    }
}
