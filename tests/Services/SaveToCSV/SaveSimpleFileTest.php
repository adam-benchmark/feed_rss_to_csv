<?php

namespace AdamKsiazekRekrutacjaHRtecTest\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\FeedElementDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveSimpleFile;
use PHPUnit\Framework\TestCase;

class SaveSimpleFileTest extends TestCase
{
    public function providerSaveSimple()
    {
        $argvMockSimple = [
            "console.php",
            "csv:simple",
            "https://blog.nationalgeographic.org/rss",
            "csv/simple_export.csv"
        ];
        return [
            ['SaveSimpleFile | done', $argvMockSimple]
        ];
    }

    /**
     * @dataProvider providerSaveSimple
     */
    public function testSaveSimple($expect, $input): void
    {
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($input);

        $feedElementDTO = new FeedElementDTO();
        $feedElementDTO->title = 'jakis title';
        $feedElementDTO->description = 'jakis description';

        $feedElements = new \ArrayObject([$feedElementDTO]);

        $saveSimpleFile = new SaveSimpleFile();
        $result = $saveSimpleFile->save($inputArgumentsDTO, $feedElements);
        $this->assertEquals($expect, $result);
    }


    public function providerSaveSimpleEmptyFeedElements()
    {
        $argvMockExtended = [
            "console.php",
            "csv:simple",
            "https://blog.nationalgeographic.org/rss",
            "csv/simple_export.csv"
        ];
        $feedElements = new \ArrayObject([]);
        return [
            ['Error | feed is empty', [$argvMockExtended, $feedElements]]
        ];
    }

    /**
     * @dataProvider providerSaveSimpleEmptyFeedElements
     */
    public function testSaveSimpleEmptyFeedElements($expect, $input)
    {
        $saveSimpleFile = new SaveSimpleFile();
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($input[0]);
        $result = $saveSimpleFile->save($inputArgumentsDTO, $input[1]);
        $this->assertEquals($expect, $result);
    }

}
