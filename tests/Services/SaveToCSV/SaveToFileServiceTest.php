<?php

namespace AdamKsiazekRekrutacjaHRtecTest\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveExtendedFile;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveSimpleFile;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveToFileService;
use PHPUnit\Framework\TestCase;

class SaveToFileServiceTest extends TestCase
{
    public function testFactorySaveExtendedFile(): void
    {
        $argvMock = [
            "console.php",
            "csv:extended",
            "",
            "csv/extended_export.csv"
        ];

        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argvMock);
        $saveToFileService = new SaveToFileService();
        $result = $saveToFileService->factory($inputArgumentsDTO);

        $this->assertInstanceOf(SaveExtendedFile::class, $result);
    }

    public function testFactorySaveSimpleFile(): void
    {
        $argvMock = [
            "console.php",
            "csv:simple",
            "",
            "csv/simple_export.csv"
        ];

        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argvMock);
        $saveToFileService = new SaveToFileService();
        $result = $saveToFileService->factory($inputArgumentsDTO);

        $this->assertInstanceOf(SaveSimpleFile::class, $result);
    }
}
