<?php declare(strict_types=1);

namespace AdamKsiazekRekrutacjaHRtecTest;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveSimpleFile;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveToFileService;
use AdamKsiazekRekrutacjaHRtec\Services\SiteData\SiteDataService;
use AdamKsiazekRekrutacjaHRtec\Services\Tasks\ReadFeed;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

/**
 * Class InvestmentTest
 */
class ReadFeedTest extends TestCase
{
    public function testReadFeedConstruct(): void
    {
        $readFeed = new ReadFeed(
            new SiteDataService,
            new SaveToFileService
        );

        $this->assertInstanceOf(ReadFeed::class, $readFeed);
    }

    /**
     * @dataProvider providerResolveDTOinput
     */
    public function testResolveInputArguments($expected, $input)
    {
        $object = new ReadFeed(
            new SiteDataService,
            new SaveToFileService
        );
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('resolveInputArguments');
        $method->setAccessible( true );
        $testResult = $method->invokeArgs( $object, array($input) );
        $this->assertInstanceOf(InputArgumentsDTO::class, $testResult);
        $this->assertEquals( $expected, $testResult->mode );
    }

    /**
     * @return array
     */
    public function providerResolveDTOinput()
    {
        $argvMockSimple = [
            "console.php",
            "csv:simple",
            "https://blog.nationalgeographic.org/rss",
            "csv/simple_export.csv"
        ];
        $argvMockExtended = [
            "console.php",
            "csv:extended",
            "https://blog.nationalgeographic.org/rss",
            "csv/extended_export.csv"
        ];
        return [
            ['csv:extended', $argvMockExtended],
            ['csv:simple', $argvMockSimple]
        ];
    }

    /**
     * @dataProvider providerResolveDTOinput
     */
    public function testResolveDTOinput($expected, $input)
    {
        $object = new ReadFeed(
            new SiteDataService,
            new SaveToFileService
        );
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('resolveDTOinput');
        $method->setAccessible( true );
        $testResult = $method->invokeArgs( $object, array($input) );
        $this->assertInstanceOf(InputArgumentsDTO::class, $testResult);
        $this->assertEquals( $expected, $testResult->mode );
    }

    public function testValidateInput()
    {
        $argvMock = [
            "console.php",
            "csv:extended",
            "https://blog.nationalgeographic.org/rss",
            "simple_export.csv"
        ];
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argvMock);
        $object = new ReadFeed(
            new SiteDataService,
            new SaveToFileService
        );
        $reflector = new ReflectionClass($object);
        $method = $reflector->getMethod('validateInput');
        $method->setAccessible( true );
        $testResult = $method->invokeArgs( $object, array($inputArgumentsDTO) );
        $this->assertEquals( true, $testResult );
    }

    /**
     * @return array
     */
    public function providerDoJob()
    {
        $argvMockSimple = [
            "console.php",
            "csv:simple",
            "https://blog.nationalgeographic.org/rss",
            "csv/simple_export.csv"
        ];
        $argvMockExtended = [
            "console.php",
            "csv:extended",
            "https://blog.nationalgeographic.org/rss",
            "csv/extended_export.csv"
        ];
        return [
            ['SaveExtendedFile | done', $argvMockExtended],
            ['SaveSimpleFile | done', $argvMockSimple]
        ];
    }

    /**
     * @dataProvider providerDoJob
     */
    public function testDoJob($expected, $input): void
    {
        $argvMock = $input;

        $saveSimpleFileMock = $this->createMock(SaveSimpleFile::class);
        $saveSimpleFileMock->method('save')
            ->willReturn($expected);

        $siteDataServiceMock = $this->createMock(SiteDataService::class);
        $siteDataServiceMock->method('init')
            ->willReturn(new \ArrayObject());

        $saveToFileServiceMock = $this->createMock(SaveToFileService::class);
        $saveToFileServiceMock->method('factory')
            ->willReturn($saveSimpleFileMock);

        $readFeed = new ReadFeed(
            $siteDataServiceMock,
            $saveToFileServiceMock
        );

        $result = $readFeed->doJob($argvMock);
        $this->assertEquals($expected, $result);
    }
}
