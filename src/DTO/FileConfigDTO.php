<?php

namespace AdamKsiazekRekrutacjaHRtec\DTO;

class FileConfigDTO
{
    /** @var bool */
    public $isFileExists;
    /** @var resource|false */
    public $fileHandler;
    /** @var string */
    public $filePath;

    /**
     * @param array $input
     *
     * @return FeedElementDTO
     */
    public static function createFromArray(array $input) : FileConfigDTO
    {
        $dto = new self();
        $dto->isFileExists = !empty($input[0]) ? $input[0] : '';
        $dto->fileHandler = !empty($input[1]) ? $input[1] : '';
        $dto->filePath = !empty($input[1]) ? $input[1] : '';

        return $dto;
    }
}