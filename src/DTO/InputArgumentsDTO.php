<?php

namespace AdamKsiazekRekrutacjaHRtec\DTO;

class InputArgumentsDTO
{
    /** @var string */
    public $executeFile;
    /** @var string */
    public $mode;
    /** @var string */
    public $siteUrl;
    /** @var string */
    public $filePath;

    /**
     * @param array $input
     *
     * @return InputArgumentsDTO
     */
    public static function createFromArray(array $input) : InputArgumentsDTO
    {
        $dto = new self();
        $dto->executeFile = !empty($input[0]) ? $input[0] : '';
        $dto->mode = !empty($input[1]) ? $input[1] : '';
        $dto->siteUrl = !empty($input[2]) ? $input[2] : '';
        $dto->filePath = !empty($input[3]) ? $input[3] : '';

        return $dto;
    }
}