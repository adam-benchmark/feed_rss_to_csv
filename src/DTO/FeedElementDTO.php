<?php

namespace AdamKsiazekRekrutacjaHRtec\DTO;

use AdamKsiazekRekrutacjaHRtec\Utils\Utils;
use SimpleXMLElement;

class FeedElementDTO
{
    /** @var string */
    public $title;
    /** @var string */
    public $description;
    /** @var string */
    public $link;
    /** @var string */
    public $pubDate;
    /** @var string */
    public $creator;

    /**
     * @param SimpleXMLElement $input
     *
     * @return FeedElementDTO
     */
    public static function createFromArray(SimpleXMLElement $input) : FeedElementDTO
    {
        $dto = new self();
        if (!empty($input)) {
            $dto->title = (string)$input->title;
            $dto->description = (string)$input->description;
            $dto->link = (string)$input->link;
            $dto->pubDate = date('d M Y H:i:s',strtotime((string)$input->pubDate));
            $workoutSlug = str_replace(
                Utils::replaceMonthName()['monthEnglish'],
                Utils::replaceMonthName()['monthPolish'],
                $dto->pubDate
            );
            $dto->pubDate = $workoutSlug;
            $dto->creator = $input->{'dc:creator'};
        }

        return $dto;
    }
}