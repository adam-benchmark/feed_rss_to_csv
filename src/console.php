<?php

namespace AdamKsiazekRekrutacjaHRtec;

use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveToFileService;
use AdamKsiazekRekrutacjaHRtec\Services\SiteData\SiteDataService;
use AdamKsiazekRekrutacjaHRtec\Services\Tasks\ReadFeed;

require __DIR__.'/../vendor/autoload.php';
/** @var ReadFeed $readFeed */
$readFeed = new ReadFeed(
    new SiteDataService,
    new SaveToFileService
);
if (!empty($argv)) {
    echo $readFeed->doJob($argv);
} else {
    echo 'type-in parameters in console';
}
echo "\n";



