<?php

namespace AdamKsiazekRekrutacjaHRtec\Interfaces;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use ArrayObject;

interface ISaveToCSV
{
    public function save(InputArgumentsDTO $inputArgumentsDTO, ArrayObject $feedElements);
}