<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SiteData;

use AdamKsiazekRekrutacjaHRtec\DTO\FeedElementDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use ArrayObject;
use SimpleXMLElement;

class SiteDataService
{
    /**
     * @param string $content
     * @return SimpleXMLElement
     * @throws \Exception
     */
    private function simpleXmlElementByContent(string $content)
    {
        /** @var SimpleXMLElement $xml */
        $xml = new SimpleXmlElement($content);

        return $xml;
    }

    /**
     * @param SimpleXMLElement $xml
     *
     * @return ArrayObject
     */
    private function extract(SimpleXMLElement $xml)
    {
        /** @var ArrayObject $feedElements */
        $feedElements = new ArrayObject();
        foreach($xml->channel->item as $entry) {
            /** @var FeedElementDTO $feedElementDTO */
            $feedElementDTO = FeedElementDTO::createFromArray($entry);
            $feedElements->append($feedElementDTO);
        }

        return $feedElements;
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     *
     * @return string
     */
    private function fileGetContents(InputArgumentsDTO $inputArgumentsDTO)
    {
        /** @var string $content */
        $content = file_get_contents($inputArgumentsDTO->siteUrl);

        return $content;
    }

    /**
     * @param string $content
     *
     * @return bool
     */
    private function isXML(string $content)
    {

        return @simplexml_load_string($content) ? true : false;
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     *
     * @return ArrayObject
     *
     * @throws \Exception
     */
    public function init(InputArgumentsDTO $inputArgumentsDTO)
    {
        /** @var ArrayObject $feedElements */
        $feedElements = new ArrayObject([]);
        if (!empty($inputArgumentsDTO->siteUrl)) {
            /** @var string $content */
            $content = $this->fileGetContents($inputArgumentsDTO);
            if ($this->isXML($content)) {
                /** @var SimpleXMLElement $xml */
                $xml = $this->simpleXmlElementByContent($content);
                /** @var ArrayObject $feedElements */
                $feedElements = $this->extract($xml);
            } else {
                echo "Error | content does not xml format\n";
            }
        }

        return $feedElements;
    }
}