<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Interfaces\ISaveToCSV;
use ArrayObject;

class ErrorFile extends SaveToFileBase implements ISaveToCSV
{
    /** @var string $result  */
    private $result = 'ErrorFile';

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param ArrayObject $feedElements
     * @return string
     */
    public function save(InputArgumentsDTO $inputArgumentsDTO, ArrayObject $feedElements)
    {

        return $this->result;
    }
}