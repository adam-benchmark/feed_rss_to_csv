<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\FeedElementDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\FileConfigDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use ArrayObject;
use Exception;

abstract class SaveToFileBase
{

    /**
     * @param FeedElementDTO $feedElementDTO
     *
     * @return array
     */
    private function ConvertDtoToArray(FeedElementDTO $feedElementDTO): array
    {
        return [
            $feedElementDTO->title ? $feedElementDTO->title : "",
            $feedElementDTO->description ? $feedElementDTO->description : "",
            $feedElementDTO->link ? $feedElementDTO->link : "",
            $feedElementDTO->pubDate ? $feedElementDTO->pubDate : "",
            $feedElementDTO->creator ? $feedElementDTO->creator : ""
        ];
    }

    /**
     * @param string $resolvefilePath
     *
     * @return bool
     */
    private function isFileExists(string $resolvefilePath)
    {
        return
            file_exists($resolvefilePath);
    }

    /**
     * @param ArrayObject $feedElements
     *
     * @return bool
     */
    protected function isCorrectFeedElements(ArrayObject $feedElements)
    {
        return $feedElements->count() ? true : false;
    }

    /**
     * @param FileConfigDTO $fileConfigDTO
     */
    protected function resolveHeaderToCSV(FileConfigDTO $fileConfigDTO)
    {
        $this->fputcsvDoIt(
            $fileConfigDTO,
            $this->getCSVTitles()
        );
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param string $fOpenMode
     *
     * @return FileConfigDTO
     */
    protected function resolveFileConfigDTO(
        InputArgumentsDTO $inputArgumentsDTO,
        string $fOpenMode
    ) {
        /** @var string $resolvefilePath */
        $resolvefilePath = $this->resolvePath($inputArgumentsDTO);
        /** @var FileConfigDTO $fileConfigDTO */
        $fileConfigDTO = FileConfigDTO::createFromArray([
            $this->isFileExists($resolvefilePath),
            $this->openOrCreateCSVFile($resolvefilePath, $fOpenMode),
            $resolvefilePath
        ]);

        return $fileConfigDTO;
    }

    /**
     * @param string $resolvefilePath
     * @param string $fOpenMode
     * 
     * @return resource|void
     */
    protected function openOrCreateCSVFile(string $resolvefilePath, string $fOpenMode)
    {
        $fileHandler = fopen(
            $resolvefilePath,
            $fOpenMode
        );
        if (!$fileHandler) {
            echo "Error | File open failed, maybe you type wrong director for file\n";
            exit;
        }

        return $fileHandler;

    }

    /**
     * @param FileConfigDTO $fileConfigDTO
     * @param ArrayObject $feedElements
     */
    protected function saveFeedElements(
        FileConfigDTO $fileConfigDTO,
        ArrayObject $feedElements
    ) {
        for($i=0;$i<$feedElements->count();$i++) {
            /** @var array $arrayRow */
            $arrayRow = $this->ConvertDtoToArray($feedElements[$i]);
            $this->fputcsvDoIt($fileConfigDTO, $arrayRow);
        }
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     *
     * @return string
     */
    protected function resolvePath(InputArgumentsDTO $inputArgumentsDTO)
    {
        return
            sprintf(
                "%s/%s",
            "public",
            $inputArgumentsDTO->filePath
        );
    }

    /**
     * @param FileConfigDTO $fileConfigDTO
     *
     * @param $arrayRow
     */
    protected function fputcsvDoIt (
        FileConfigDTO $fileConfigDTO,
        $arrayRow
    ) {
        fputcsv($fileConfigDTO->fileHandler, $arrayRow, ";", "\"", "\\");
    }

    /**
     * @param FileConfigDTO $fileConfigDTO
     */
    protected function doFClose(FileConfigDTO $fileConfigDTO)
    {
        fclose($fileConfigDTO->fileHandler);
    }

    /**
     * Get title of columns
     * for csv file
     *
     * @return string[]
     */
    protected function getCSVTitles()
    {
        return [
            'title',
            'description',
            'link',
            'pubDate',
            'creator'
        ];
    }
}