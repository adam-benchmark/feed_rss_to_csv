<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\FileConfigDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Interfaces\ISaveToCSV;
use ArrayObject;

class SaveExtendedFile extends SaveToFileBase implements ISaveToCSV
{
    const FILE_APPEND_OR_CREAT_MODE = 'a';

    /** @var string $result  */
    private $result = 'SaveExtendedFile | done';

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param ArrayObject $feedElements
     */
    private function process(
        InputArgumentsDTO $inputArgumentsDTO,
        ArrayObject $feedElements
    ) {
        /** @var FileConfigDTO $fileConfigDTO */
        $fileConfigDTO = $this->resolveFileConfigDTO (
            $inputArgumentsDTO,
            self::FILE_APPEND_OR_CREAT_MODE
        );
        if (!$fileConfigDTO->isFileExists) {
            $this->resolveHeaderToCSV($fileConfigDTO);
        }
        $this->saveFeedElements($fileConfigDTO, $feedElements);
        $this->doFClose($fileConfigDTO);
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param ArrayObject $feedElements
     *
     * @return string
     */
    public function save(
        InputArgumentsDTO $inputArgumentsDTO,
        ArrayObject $feedElements
    ) {
        if ($this->isCorrectFeedElements($feedElements)) {
            $this->process($inputArgumentsDTO, $feedElements);
        } else {
            $this->result = 'Error | feed is empty';
        }

        return $this->result;
    }
}