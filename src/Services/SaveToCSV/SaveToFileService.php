<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Interfaces\ISaveToCSV;

class SaveToFileService
{
    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     *
     * @return ErrorFile|SaveExtendedFile|SaveSimpleFile|ISaveToCSV
     */
    public function factory(InputArgumentsDTO $inputArgumentsDTO)
    {
        /** @var ISaveToCSV $resultSaveFile */
        $resultSaveFile = new ErrorFile();
        switch ($inputArgumentsDTO->mode)
        {
            case 'csv:simple':
                $resultSaveFile = new SaveSimpleFile();
                break;
            case 'csv:extended':
                $resultSaveFile = new SaveExtendedFile();
                break;
        }

        return $resultSaveFile;
    }
}