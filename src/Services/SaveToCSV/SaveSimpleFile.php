<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV;

use AdamKsiazekRekrutacjaHRtec\DTO\FileConfigDTO;
use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Interfaces\ISaveToCSV;
use ArrayObject;

class SaveSimpleFile extends SaveToFileBase implements ISaveToCSV
{
    const FILE_WRITE_OR_CREATE_MODE = 'w';

    /** @var string $result  */
    private $result = 'SaveSimpleFile | done';

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param ArrayObject $feedElements
     */
    private function process(
        InputArgumentsDTO $inputArgumentsDTO,
        ArrayObject $feedElements
    ) {
        /** @var FileConfigDTO $fileConfigDTO */
        $fileConfigDTO = $this->resolveFileConfigDTO (
            $inputArgumentsDTO,
            self::FILE_WRITE_OR_CREATE_MODE
        );
        $this->resolveHeaderToCSV($fileConfigDTO);
        $this->saveFeedElements($fileConfigDTO, $feedElements);
        $this->doFClose($fileConfigDTO);
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     * @param ArrayObject $feedElements
     * @return string
     */
    public function save(
        InputArgumentsDTO $inputArgumentsDTO,
        ArrayObject $feedElements
    ) {
        if ($this->isCorrectFeedElements($feedElements)) {
            $this->process($inputArgumentsDTO, $feedElements);
        } else {
            $this->result = 'Error | feed is empty';
        }

        return $this->result;
    }
}