<?php

namespace AdamKsiazekRekrutacjaHRtec\Services\Tasks;

use AdamKsiazekRekrutacjaHRtec\DTO\InputArgumentsDTO;
use AdamKsiazekRekrutacjaHRtec\Interfaces\ISaveToCSV;
use AdamKsiazekRekrutacjaHRtec\Services\SaveToCSV\SaveToFileService;
use AdamKsiazekRekrutacjaHRtec\Services\SiteData\SiteDataService;
use ArrayObject;


class ReadFeed
{
    const CSV_COLON = "csv:";
    const CHECK_SUBDOMAIN = "https://blog.nationalgeographic.org";
    /** @var SiteDataService $siteDataService */
    private $siteDataService;
    /** @var SaveToFileService $saveToFileService */
    private $saveToFileService;

    /**
     * Constructor
     *
     * @param SiteDataService $siteDataService
     * @param SaveToFileService $saveToFileService
     *
     * @return void
     */
    public function __construct(
        SiteDataService $siteDataService,
        SaveToFileService $saveToFileService
    ) {
        $this->siteDataService = $siteDataService;
        $this->saveToFileService = $saveToFileService;
    }

    /**
     * @param InputArgumentsDTO $inputArgumentsDTO
     *
     * @return bool
     */
    private function validateInput(InputArgumentsDTO $inputArgumentsDTO) : bool
    {
        /** @var bool $validateResult */
        $validateResult = !empty($inputArgumentsDTO->mode)
            && strpos($inputArgumentsDTO->mode, self::CSV_COLON) !== false
            ? true : false;
        $validateResult = $validateResult
            && strpos($inputArgumentsDTO->siteUrl, self::CHECK_SUBDOMAIN) !== false
            ? true : false;

        return $validateResult;
    }

    /**
     * @param array $argv
     *
     * @return InputArgumentsDTO
     */
    private function resolveDTOinput(array $argv) : InputArgumentsDTO
    {
        /** @var InputArgumentsDTO $inputArgumentsDTO */
        $inputArgumentsDTO = InputArgumentsDTO::createFromArray($argv);

        return  $inputArgumentsDTO;
    }

    /**
     * @param array $argv
     *
     * @return InputArgumentsDTO
     */
    private function resolveInputArguments(array $argv): InputArgumentsDTO
    {
        /** @var InputArgumentsDTO $inputArgumentsDTO */
        $inputArgumentsDTO = $this->resolveDTOinput($argv);
        /** @var bool $validateInput */
        $validateInput = $this->validateInput($inputArgumentsDTO);

        return $validateInput ? $inputArgumentsDTO : InputArgumentsDTO::createFromArray([]);
    }

    /**
     * @param array $argv
     *
     * @return string
     *
     * @throws \Exception
     */
    public function doJob(array $argv): string
    {
        /** @var InputArgumentsDTO $inputArgumentsDTO */
        $inputArgumentsDTO = $this->resolveInputArguments($argv);
        /** @var ArrayObject $feedElements */
        $feedElements = $this->siteDataService->init($inputArgumentsDTO);
        /** @var ISaveToCSV $resultSaveFileFactory */
        $resultSaveFileFactory = $this->saveToFileService->factory($inputArgumentsDTO);

        /** @var string $resultFetch */
        $resultFetch = $resultSaveFileFactory->save($inputArgumentsDTO, $feedElements);

        return $resultFetch;
    }
}