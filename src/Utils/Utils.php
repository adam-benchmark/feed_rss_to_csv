<?php

namespace AdamKsiazekRekrutacjaHRtec\Utils;

class Utils
{
    /**
     * @return array[]
     */
    public static function replaceMonthName()
    {
        /** @var array $monthEnglish */
        $monthEnglish = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        /** @var array $monthPolish */
        $monthPolish = array("stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia",
            "września", "października", "listopada", "grudnia");

        return [
            'monthEnglish' => $monthEnglish,
            'monthPolish' => $monthPolish
        ];
    }
}